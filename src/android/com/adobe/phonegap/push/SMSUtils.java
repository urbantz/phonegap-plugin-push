package com.adobe.phonegap.push;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsManager;
import java.util.ArrayList;
import java.lang.Thread;
import android.util.Log;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

public class SMSUtils extends BroadcastReceiver {

    public static final String SENT_SMS_ACTION_NAME = "SMS_SENT";
    public static final String DELIVERED_SMS_ACTION_NAME = "SMS_DELIVERED";
    private static final String LOG_TAG = "SMS_SENDER";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Detect l'envoie de sms
        if (intent.getAction().equals(SENT_SMS_ACTION_NAME)) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Log.i(LOG_TAG, "SMS sent");
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Log.i(LOG_TAG, "Generic failure");
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Log.i(LOG_TAG, "No service");
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Log.i(LOG_TAG, "Null PDU");
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Log.i(LOG_TAG, "Radio off");
                    break;
            }
        } else if (intent.getAction().equals(DELIVERED_SMS_ACTION_NAME)) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Log.i(LOG_TAG, "Sending OK");
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i(LOG_TAG, "Sending canceled");
                    break;
            }
        }
    }

    public static boolean canSendSMS(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    public static void sendSMS(final Context context, String phoneNumber, String message) {

        if (!canSendSMS(context)) {
            Log.i(LOG_TAG, "Cannot send SMS"); return;
        }

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT_SMS_ACTION_NAME), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED_SMS_ACTION_NAME), 0);

        final SMSUtils smsUtils = new SMSUtils();
        //register for sending and delivery
        context.registerReceiver(smsUtils, new IntentFilter(SMSUtils.SENT_SMS_ACTION_NAME));
        context.registerReceiver(smsUtils, new IntentFilter(SMSUtils.DELIVERED_SMS_ACTION_NAME));

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(message);

        ArrayList<PendingIntent> sendList = new ArrayList<PendingIntent>();
        sendList.add(sentPI);

        ArrayList<PendingIntent> deliverList = new ArrayList<PendingIntent>();
        deliverList.add(deliveredPI);

        sms.sendMultipartTextMessage(phoneNumber, null, parts, sendList, deliverList);
        
        Thread thread = new Thread() {
            public void run () {
                Looper.prepare();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        context.unregisterReceiver(smsUtils);
                        Looper.myLooper().quit();
                    }
                }, 10000);

                Looper.loop();
            }
        };

        thread.start();
    }
}